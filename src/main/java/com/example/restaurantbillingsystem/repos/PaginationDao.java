package com.example.restaurantbillingsystem.repos;

import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PaginationDao extends PagingAndSortingRepository<Menu, Long> {
}
