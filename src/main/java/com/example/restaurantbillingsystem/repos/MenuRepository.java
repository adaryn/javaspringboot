package com.example.restaurantbillingsystem.repos;

import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import com.example.restaurantbillingsystem.com.example.jpa.model.Menu_Orders;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu,Long> {
//    @Override

    @Transactional
    @Modifying
    @Query("delete from Menu_Orders mo where mo.menu = ?1")
    public void deleteAllReferenced(Menu menu);

    @Query("select o from Menu_Orders mo join Order o where mo.menu = ?1")
    List<Order> findOrderByMenu(Menu menu);

//    Page<Menu> findAll(Pageable pageable);
}
