package com.example.restaurantbillingsystem.repos;

import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.com.example.jpa.model.Menu_Orders;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order,String> {
//    @Query(value = "select * from Order o, Menu m, Menu_Orders mo where m.id = mo.id and m.id = ?1")
//      @Query(”SELECT u FROM UserEntity u WHERE u.emailVerificationStatus = 'true' ”)
    @Query("SELECT o from Order o, Menu m, Menu_Orders mo where m.id = ?1 and m.id = mo.menu and o.id = mo.order")
     List<Order> findOrderByMenuId(Long id);

    @Query("SELECT o from Order o where o.id = ?1")
    Order getById(String id);
}
