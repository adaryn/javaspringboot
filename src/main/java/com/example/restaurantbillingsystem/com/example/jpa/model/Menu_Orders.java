package com.example.restaurantbillingsystem.com.example.jpa.model;


import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Data
@Entity
@IdClass(Menu_OrdersPk.class)
@Table(name = "Menu_Orders")
public class Menu_Orders {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
    @Id
    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "menus_id",nullable = false)
    private Menu menu;
    @Id
    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "orders_id",nullable = false)
    private Order order;

}
