package com.example.restaurantbillingsystem.com.example.jpa.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Menu_OrdersPk implements Serializable {
    private Long menu;
    private String order;

}
