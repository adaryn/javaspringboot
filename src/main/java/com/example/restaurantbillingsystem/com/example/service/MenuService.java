package com.example.restaurantbillingsystem.com.example.service;


import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.repos.MenuRepository;
import com.example.restaurantbillingsystem.repos.PaginationDao;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MenuService {

    @Autowired
    private PaginationDao paginationDao;

    private final MenuRepository menuRepository;

    public List<Menu> findAll() {
        return menuRepository.findAll();
    }
    public Optional<Menu> findById(Long id) {
        return menuRepository.findById(id);
    }

    public Menu save(Menu menu) {
        return menuRepository.save(menu);
    }

    public void deleteById(Long id) {
        menuRepository.deleteById(id);
    }

    public Page<Menu> findJsonDataByCondition(String orderBy, String direction, int page, int size) {
       Sort sort = null;
       if (direction.equals("ASC")) {
           sort = new Sort(new Sort.Order(Sort.Direction.ASC, orderBy));
       }
       if (direction.equals("DESC")) {
           sort = new Sort(new Sort.Order(Sort.Direction.DESC, orderBy));
       }
       Pageable pageable = new PageRequest(page, size, sort);

       Page<Menu> data = paginationDao.findAll(pageable);

       return data;


    }

//    public Page<Menu> findPaginated(Pageable pageable) {
//        int pageSize = pageable.getPageSize();
//        int currentPage = pageable.getPageNumber();
//        int startItem = currentPage * pageSize;
//        List<Menu> list;
//        if (books.size() < startItem) {
//            list = Collections.emptyList();
//        }
//
//    }

}
