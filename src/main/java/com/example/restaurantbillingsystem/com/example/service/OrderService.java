package com.example.restaurantbillingsystem.com.example.service;


import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import com.example.restaurantbillingsystem.repos.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;

    public List<Order> findAll() {
        return orderRepository.findAll();
    }
}
