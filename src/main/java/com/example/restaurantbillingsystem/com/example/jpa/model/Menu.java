package com.example.restaurantbillingsystem.com.example.jpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Data
@EntityListeners(AuditingEntityListener.class)
@Entity

public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;

    private Date createdAt;



    @JsonIgnore
    @Transient
    @NotNull(message = "Hey, you have to order something!!")
    private Set<Order> orders = new HashSet<>();

    //@Pattern(regexp = "^[A-Z]",message = "Doesn't your name start with capitalized letter?")

    @NotBlank(message = "Please enter your name")
    @Column(name = "name1")
    private String waiter_name;

//    private Integer total_price;

    @PrePersist
    void createdAt(){
        this.createdAt = new Date();
    }


}
