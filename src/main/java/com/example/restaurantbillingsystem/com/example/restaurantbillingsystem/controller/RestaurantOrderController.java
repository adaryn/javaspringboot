package com.example.restaurantbillingsystem.com.example.restaurantbillingsystem.controller;



import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.com.example.jpa.model.Menu_Orders;
import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import com.example.restaurantbillingsystem.com.example.jpa.model.User;
import com.example.restaurantbillingsystem.com.example.service.UserService;
import com.example.restaurantbillingsystem.repos.MenuRepository;
import com.example.restaurantbillingsystem.repos.Menu_OrdersRepository;
import com.example.restaurantbillingsystem.repos.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.example.restaurantbillingsystem.com.example.jpa.model.Order.Type;

import javax.validation.Valid;


@Slf4j
@Controller
@RequestMapping("/order")
@SessionAttributes("menu")
public class RestaurantOrderController {

    @ModelAttribute(name = "menu")
    public Menu menu() {
        return new Menu();
    }


    private final OrderRepository orderRepository;
    private MenuRepository menuRepository;
    @Autowired(required = true)
    private Menu_OrdersRepository menuOrdersRepository;

    @Autowired
    private UserService userService;




    @Autowired
    public RestaurantOrderController(OrderRepository orderRepository, MenuRepository menuRepository) {
        this.orderRepository = orderRepository;
        this.menuRepository = menuRepository;
        this.menuOrdersRepository = menuOrdersRepository;

    }

    private List<Order> filterByType(List<Order> orders, Type type){


        return orders.stream()
                .filter(x -> x.getType().equals(type))
                .collect(Collectors.toList());
    }


    @ModelAttribute("allOrders")
    public List<Order> populateOrders(){

        List<Order> orders = new ArrayList<>();

        System.out.println(orderRepository.count());

        orderRepository.findAll().forEach(i -> orders.add(i));

        System.out.println("The number of order is " + orders.size());


        return orders;

    }

    @GetMapping
    public String orderForm(Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.isAuthenticated();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);

        model.addAttribute("menu", new Menu());

        return "order";
    }

    @PostMapping
    public String processDesign(@ModelAttribute @Valid Menu menu, Errors errors ){

        if (errors.hasErrors()) {
            return "order";
        }



        System.out.println("menu = " + menu.getOrders());

        menu = menuRepository.save(menu);

        Menu_Orders menuOrders = new Menu_Orders();

        for (Order i : menu.getOrders()){
            menuOrders.setMenu(menu);
            menuOrders.setOrder(i);
            menuOrdersRepository.save(menuOrders);

        }

//        menuOrdersRepository.save(menuOrders);




        System.out.println("menu = " + menu.getOrders().size());

        return "redirect:/";


    }

}
