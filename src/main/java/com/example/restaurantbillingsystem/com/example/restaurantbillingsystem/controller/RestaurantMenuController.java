package com.example.restaurantbillingsystem.com.example.restaurantbillingsystem.controller;


import com.example.restaurantbillingsystem.com.example.jpa.model.*;
import com.example.restaurantbillingsystem.com.example.service.UserService;
import com.example.restaurantbillingsystem.repos.MenuRepository;
import com.example.restaurantbillingsystem.repos.Menu_OrdersRepository;
import com.example.restaurantbillingsystem.repos.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.expression.Lists;
import org.springframework.web.bind.annotation.ResponseStatus;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping(value = "/menus", produces = "application/json")
@CrossOrigin(origins = "*")

public class RestaurantMenuController {

    @Autowired
    MenuRepository menuRepository;
    Menu_OrdersRepository menuOrdersRepository;
    OrderRepository orderRepository;
    @Autowired
    private UserService userService;
    @Autowired
    public RestaurantMenuController( MenuRepository menuRepository, Menu_OrdersRepository menu_ordersRepository, OrderRepository orderRepository) {
        this.menuRepository = menuRepository;
        this.menuOrdersRepository = menu_ordersRepository;
        this.orderRepository = orderRepository;
    }

    //Get all
    @GetMapping
    public String getAllMenus(Model model) {
        model.addAttribute("menus",menuRepository.findAll());
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.isAuthenticated();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);

        return "menu";

    }

//    @GetMapping("/api")
//    public Iterable<Menu> recentMenus(){
//        PageRequest page = PageRequest.of(0, 12 , Sort.by("createdAt").descending());
//        return menuRepository.findAll(page).getContent();
//
//    }
    @GetMapping("/api/{id}")
    public Menu menuById(@PathVariable("id") Long id){
        Optional<Menu> optMenu = menuRepository.findById(id);
        if (optMenu.isPresent()) {
            return optMenu.get();
        }
        return null;
    }

    //create
    @PostMapping
    public Menu createMenu(@Valid @RequestBody Menu menu) {

        return menuRepository.save(menu);
    }



    @GetMapping("/{id}")
    public String getMenyById(@PathVariable(value = "id") Long menuId, Model model) {

        Menu menu = menuRepository.findById(menuId).orElse(null);
        // order =


        model.addAttribute("menu", menu);

//        Order orders = orderRepository.findById(menuOrders.getOrder().getId()).orElse(null);
//        model.addAttribute("orders","order");

        return "menu-show";


    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable(value = "id") Long id, Model model) {
        Menu menu = menuRepository.findById(id).orElse(null);
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(i -> orders.add(i));
        model.addAttribute("orders", orders);
        model.addAttribute("menu",menu);
        return "update-menu";
    }

    @PostMapping("/update/{id}")
    public String updateMenu(@PathVariable("id") Long id, @ModelAttribute @Valid Menu menu,Model model, Errors errors) {

        if (errors.hasErrors()) {
            return "update-menu";
        }



        menuRepository.save(menu);


        return "redirect:/";
    }
    @GetMapping("/delete/{id}")
    public String deleteMenu(@PathVariable(value = "id") Long id, Model model) {

        Menu menu = menuRepository.findById(id).orElse(null);


        menuRepository.deleteAllReferenced(menu);

        menuRepository.delete(menu);

        model.addAttribute("menu",menuRepository.findAll());

        return "redirect:/";
    }


}
