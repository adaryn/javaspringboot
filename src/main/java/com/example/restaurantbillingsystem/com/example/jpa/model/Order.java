package com.example.restaurantbillingsystem.com.example.jpa.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data

@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@Entity
@Table(name = "Order_Menu")
public class Order {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;
//    private final Date createdAt;
    @Column(name = "name1")
    private final String name;
    private final String description;
    private final Integer price;
    @Column(name = "type1")
    private final String type;
    public static enum Type{
        SALAD,DRINK,SOUP,BREAKFAST,LUNCH,SNACK
    }

//    @PrePersist
//    void createdAt(){
//        this.createdAt = new Date();
//    }

    @Override
    public String toString(){
        return getId().toString();
    }
}
