package com.example.restaurantbillingsystem.com.example.restaurantbillingsystem.controller;


import com.example.restaurantbillingsystem.com.example.jpa.model.Menu;
import com.example.restaurantbillingsystem.com.example.jpa.model.Menu_Orders;
import com.example.restaurantbillingsystem.com.example.jpa.model.Order;
import com.example.restaurantbillingsystem.com.example.service.MenuService;
import com.example.restaurantbillingsystem.com.example.service.OrderService;
import com.example.restaurantbillingsystem.com.example.service.UserService;
import com.example.restaurantbillingsystem.repos.MenuRepository;
import com.example.restaurantbillingsystem.repos.Menu_OrdersRepository;
import com.example.restaurantbillingsystem.repos.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
@Slf4j
@RequiredArgsConstructor

public class RestaurantApiController {

    MenuRepository menuRepository;
    Menu_OrdersRepository menuOrdersRepository;
    OrderRepository orderRepository;
    private UserService userService;
    private MenuService menuService;
    private OrderService orderService;

    @Autowired
    public RestaurantApiController(MenuRepository menuRepository, Menu_OrdersRepository menuOrdersRepository,
                                   OrderRepository orderRepository, UserService userService,
                                   MenuService menuService, OrderService orderService) {
        this.menuRepository = menuRepository;
        this.menuOrdersRepository = menuOrdersRepository;
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.menuService = menuService;
        this.orderService = orderService;
    }

    @GetMapping(params = {"orderBy", "direction", "page", "size"})
    public Iterable<Menu> recentMenus(@RequestParam("orderBy") String orderBy, @RequestParam("direction") String direction,  @RequestParam("page") int page,
                                      @RequestParam("size") int size) {


        Page<Menu> list = menuService.findJsonDataByCondition(orderBy, direction, page, size);

//        PageRequest page = PageRequest.of(0, 5, Sort.by("createdAt").descending());
//        return menuRepository.findAll(page).getContent();


        return list;
//        return ResponseEntity.ok(menuRepository.findAll());
    }


    @GetMapping("/order/{id}")
    public ResponseEntity<List<Order>> orderWithId(@PathVariable("id") Long id){
        return ResponseEntity.ok(orderRepository.findOrderByMenuId(id));
    }

    @GetMapping()
    public Iterable<Menu> recentMenus() {
        PageRequest page = PageRequest.of(0, 5, Sort.by("createdAt").descending());
        return menuRepository.findAll(page).getContent();
//        return ResponseEntity.ok(menuRepository.findAll());
    }

    //    public Page<Menu> findPaginated(Pageable pageable) {
//        int pageSize = pageable.getPageSize();
//        int currentPage = pageable.getPageNumber();
//        int startItem = currentPage * pageSize;
//        List<Menu> list;
//        if (books.size() < startItem) {
//            list = Collections.emptyList();
//        }
//
//    }

    @GetMapping("/order")
    public ResponseEntity<List<Order>> recentOrders() {
        return ResponseEntity.ok(orderRepository.findAll());
    }

    @GetMapping("/{id}")
    public Menu menuById(@PathVariable("id") Long id) {
        Optional<Menu> optMenu = menuRepository.findById(id);
        if (optMenu.isPresent()) {
            return optMenu.get();
        }
        return null;
    }

    @PostMapping(consumes = "application/json")
    public Menu createMenu(@RequestBody Menu menu) {
        System.out.println("Orders are " + menu.getOrders());
        Menu_Orders menuOrders = new Menu_Orders();
        System.out.println("Menu is : " + menu);


        for (Order i : menu.getOrders()) {
            menuOrders.setMenu(menu);
            menuOrders.setOrder(i);
            menuOrdersRepository.save(menuOrders);

        }

//        for (int i = 0; i< order_ids.length; i++) {
//            menuOrders.setMenu(menu);
//            menuOrders.setOrder(orderRepository.getById(order_ids[i]));
//            menuOrdersRepository.save(menuOrders);
//        }



        return menuRepository.save(menu);

    }

    @PutMapping("/{id}")
    public Menu updateMenu(@RequestBody Menu menu) {

        menu = menuRepository.save(menu);

        Menu_Orders menuOrders = new Menu_Orders();

        for (Order i : menu.getOrders()){
            menuOrders.setMenu(menu);
            menuOrders.setOrder(i);
            menuOrdersRepository.save(menuOrders);

        }


        return menu;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        System.out.println("RestaurantApiController.delete");
        if (!menuService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }
        Menu menu = menuRepository.findById(id).orElse(null);
        menuRepository.deleteAllReferenced(menu);
        menuRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
