package com.example.restaurantbillingsystem.com.example.restaurantbillingsystem.controller;

import com.example.restaurantbillingsystem.com.example.jpa.model.User;
import com.example.restaurantbillingsystem.com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @GetMapping("/")
    public String home(Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.isAuthenticated();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);

        return "index";
    }
    @GetMapping("/menuView")
    public String homeView(Model model){

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        auth.isAuthenticated();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);

        return "menus";
    }
}
