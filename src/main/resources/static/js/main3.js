function getIndex(list,id) {
    for (var i=0; i<list.length; i++) {
        if (list[i].id === id) {
            return i;
        }
    }
    return -1;
}




var messageApi = Vue.resource('/api{/id}')
var messageOrderApi = Vue.resource('/api/order{/id}')

Vue.component('message-form', {
    props:['messages','messageAttr','orders2'],
    data: function () {
        return{
            waiter_name: '',
            id: '',
            orders: []
        }
    },
    watch: {
        messageAttr: function(newVal, oldVal) {
            this.waiter_name = newVal.waiter_name;
            this.id = newVal.id;
        }
    },
    template:
    '<div>' +
    '<input type="text" placeholder="Write something" v-model="waiter_name"/>' +

    '<div v-for="order in orders2">' +
    '<input type="checkbox" v-model="orders">' +
    '<span>{{ order.name }}</span>' +
    '</div>'

    +
    '<input type="button" value="Save" @click="save">' +
    '</div>',
    methods: {
        save: function(){
            var message = { waiter_name: this.waiter_name, orders:this.orders };
            if (this.id) {
                messageApi.update({id:this.id}, message).then(result =>
                    result.json().then(data => {

                        var index = getIndex(this.messages, data.id);
                        this.messages.splice(index,1,data);
                        this.waiter_name = '';
                        this.id = '';

                    })
                 )
            }
            else {


                messageApi.save({}, message).then(result =>
                result.json().then(data => {
                    this.messages.push(data);
                this.waiter_name = ''
            })
            )
            }
        }
    }
});


Vue.component('message-order',{
    props:['order'],
    template: '<div>' +
    '{{ order.name }}' +
    '</div>'
})


Vue.component('message-row',{
    props: ['message','editMethod', 'messages'],
    template: '<div>' +
    '<i>({{ message.id }})</i> {{ message.waiter_name }}' +
    '<span style="position: absolute; right: 0">' +
    '<input type="button" value="Edit" @click="edit"/>' +
    '<input type="button" value="X" @click="del"/>' +
    '</span>' +
    '</div>',
    methods: {
        edit:function() {
            this.editMethod(this.message)
        },
        del: function () {
            messageApi.remove({id: this.message.id}).then(result => {
                if (result.ok) {
                this.messages.splice(this.messages.indexOf(this.message), 1)
            }
        })
        }
    }
})



Vue.component('messages-list',{
    props: ['messages','orders'],
    data: function() {
        return {
            message: null,
            order: null
        }
    },
    template: '<div style="position: relative; width: 300px;">' +
    '<message-form :messages="messages" :messageAttr="message" :orders2="orders"/>' +
    '<message-row v-for="message in messages" :key="message.id" :message="message"' +
    ' :editMethod="editMethod" :messages="messages">{{ message.text }}</message-row>' +
    // '<message-order v-for="order in orders" :key="order.id" :order="order"/>' +
    '</div>',
    created: function () {
        messageApi.get().then(result =>
            result.json().then(data =>
                data.forEach(message => this.messages.push(message))
         )
    );


        messageOrderApi.get().then(result =>
            result.json().then(data =>
                data.forEach(order => this.orders.push(order))
            )
        )
    },
    methods:{
        editMethod: function(message) {
            this.message = message;
        }
    }
});




var app = new Vue({
    el: '#app',
    template: '<messages-list :messages="messages" :orders="orders"/>',
    data: {
        messages: [],
        orders: []
    }
});