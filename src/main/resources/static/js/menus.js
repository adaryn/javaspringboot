var menus = [];
var orders = [];

var direction = "ASC"
var page = 0
var size = 5
var num = 0




function findMenu(menuId) {
    return menus[findMenuKey(menuId)]
}

function findMenuKey (menuId) {
    for (var key = 0; key < menus.length; key++) {
        if (menus[key].id == menuId) {
            return key;
        }

    }

}
// orderBy=id&direction=ASC&page=1&size=2
var menuService = {
    findAll(fn, param){
        axios
            .get('/api', {
                params: param
            })
            .then(response => fn(response))
            .catch(error => console.log(error))
    },

    findById(id, fn) {
        axios
            .get('/api/' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    create(menu) {
        axios
            .post('/api', menu)



    },
    update(id, menu, fn) {
        axios
            .put('/api/' + id, menu)
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    deleteM(id, fn) {
        axios
            .delete('/api/' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    }
};

var orderService = {
    findAll(fn) {
        axios
            .get('/api/order')
            .then(response => fn(response))
            .catch(error => console.log(error))
    },
    findById(id,fn) {
        axios
            .get('/api/order/' + id)
            .then(response => fn(response))
            .catch(error => console.log(error))
    }
};

Vue.component('order-post',{
    props: {
        menu_id : Number
    },
    data : function() {
        return{
            orders:[]
        }
    },
    template:'<div>' +
    '<li v-for="order in orders">' +
    '{{ order.name }}' +
    '</li>' +
    '</div>',

    mounted() {
        orderService.findById(this.menu_id, r => {this.orders = r.data; orders = r.data})
    }
})
var List = Vue.extend({
    template: '#menu-list',

    data: function () {
        return {
            menus: [], searchKey: '', orders:[], param:{}, id: ''
        };
    },

    methods:{
        changeParams: function (orderName) {


            param.orderBy = orderName


            if (param.direction == "ASC"){
                this.direction = "DESC";
                param.direction = "DESC";


            }
            else {
                this.direction = "ASC";
                param.direction = "ASC";


            }
            menuService.findAll(r => {this.menus = r.data.content; menus = r.data.content}, param)

        },
        changePage(pageDirection) {
            if (pageDirection == "Next") {


                if (param.page +1 > menus.length / size) {
                    param.page -=1
                }
                else {
                    param.page += 1
                }
            }
            else {
                if (param.page - 1 < 0 ) {
                    param.page == 0
                }
                else {
                    param.page -=1
                }

            }
            menuService.findAll(r => {this.menus = r.data.content; menus = r.data.content}, param)

        }
    },
    computed: {
        filteredMenus() {
            return this.menus.filter((menu) => {
                return menu.id.toString().indexOf(this.searchKey) > -1
                || menu.waiter_name.indexOf(this.searchKey) > -1
            })
        }

    },
    mounted() {
        param = {
            orderBy:"id",
            direction: direction,
            page: page,
            size: size
        };
        menuService.findAll(r => {this.menus = r.data.content; menus = r.data.content}, param)
        orderService.findById(this.id, r => {this.orders = r.data; orders = r.data})
    },

})


var Menu = Vue.extend({
    template: '#menu',
    data: function () {
        return  {
            menu: findMenu(this.$route.params.menu_id),
            orders: []
        };
    },
    mounted() {
        orderService.findById(this.$route.params.menu_id, r=> {this.orders = r.data; orders = r.data})
    }
});



var MenuEdit = Vue.extend({
    template: '#menu-edit',
    data: function () {
        return {
            menu: findMenu(this.$route.params.menu_id), orders:[]

        };
    },
    methods: {
        updateMenu: function () {
            menuService.update(this.menu.id, this.menu, r => router.push('/'))

        }
    }
});

var MenuDelete = Vue.extend({
    template: '#menu-delete',
    data: function () {
        return {
            menu: findMenu(this.$route.params.menu_id)
        };
    },
    methods: {
        deleteMenu: function () {

            menuService.deleteM(this.menu.id, r => router.push('/'))


        }
    }
});




var AddMenu = Vue.extend({
    template: '#add-menu',
    data() {
        return {
            menu: { orders : [], waiter_name: ''},

            order_id :[],

            orders2: []
        };
    },
    computed: {
        displayOrders(){
            return this.orders2;
        }
    },
    mounted() {
        orderService.findAll(r => {this.orders2 = r.data; orders2 = r.data})
    },

    methods: {

        createMenu() {
            console.log(this.menu)
            menuService.create(this.menu)
        }
    }
});


var router = new VueRouter({
    routes: [
        { path:'/',component: List},
        { path: '/menu/:menu_id', component: Menu, name: 'menu' },
        { path: '/add-menu', component: AddMenu},
        { path: '/menu/:menu_id/edit', component: MenuEdit, name: 'menu-edit'},
        { path: '/menu/:menu_id/delete', component: MenuDelete, name: 'menu-delete'}

    ]
})


new Vue({
    router
}).$mount('#app')


